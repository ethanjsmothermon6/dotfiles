#!/usr/bin/env bash 

festival --tts $HOME/.config/qtile/welcome_msg &
lxsession &
picom &
nitrogen --restore &
/usr/bin/emacs --daemon &
volumeicon &
nm-applet &
flameshot&
xfce4-clipman &
brave &
telegram-desktop &
wmctrl -r 'Telegram' -t 2 &
xfce4-power-manager &
